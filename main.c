/*
 * dumpACPI
 *
 * Idea taken from PHPdev32's MacIASL.
 *
 * blackosx
 * v0.1
 */

#include <CoreFoundation/CoreFoundation.h>
#include <IOKit/IOKitLib.h>
#include <stdio.h>
#include <stdlib.h>

// Create a CFString from a variable argument list
// Listing 3 - from
// https://developer.apple.com/library/mac/documentation/corefoundation/Conceptual/CFStrings/Articles/CreatingAndCopying.html
void show(CFStringRef formatString, ...) {
    CFStringRef resultString;
    CFDataRef data;
    va_list argList;
    
    va_start(argList, formatString);
    resultString = CFStringCreateWithFormatAndArguments(NULL, NULL, formatString, argList);
    va_end(argList);
    
    data = CFStringCreateExternalRepresentation(NULL, resultString, CFStringGetSystemEncoding(), '?');
    
    if (data != NULL) {
        printf ("%.*s\n\n", (int)CFDataGetLength(data), CFDataGetBytePtr(data));
        CFRelease(data);
    }
    
    CFRelease(resultString);
}

int main(int argc, char ** argv)
{
    io_service_t platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("AppleACPIPlatformExpert"));
    
    if (platformExpert){
        // create a Core Foundation container representing the ACPI Tables
        CFDictionaryRef acpiTables = IORegistryEntryCreateCFProperty(platformExpert, CFSTR("ACPI Tables"), kCFAllocatorDefault, 0);
        
        if (acpiTables) {
            
            // How many keys are there?
            //long int numTables = CFDictionaryGetCount(acpiTables);
            //printf("Number of ACPI Tables found=%lu\n",numTables);
			
            // iterate through the dictionary
            //CFDictionaryApplyFunction(acpiTables, printKeys, NULL);
            
            CFDataRef xmlPropertyListData;
            CFStringRef xmlAsString;
            
            // Now create a "property list", which is a flattened, XML version of the dictionary
            xmlPropertyListData = CFPropertyListCreateData(NULL, acpiTables, 100, 0, NULL);
            
            // The return value is a CFData containing the XML file; show the data
            xmlAsString = CFStringCreateFromExternalRepresentation(NULL, xmlPropertyListData, kCFStringEncodingUTF8);
            show(CFSTR("%@"), xmlAsString);
			
            CFRelease(acpiTables);
            CFRelease(xmlAsString);
            CFRelease(xmlPropertyListData);
        }
        IOObjectRelease(platformExpert);
    }
	return 0;
}

